<?php

namespace App\Contracts\Advertisement;

interface BannerContract
{
    const IMAGE = 'image';
    const NAME = 'name';
    const LINK = 'link';

    const FILLABLE = [
        self::NAME,
        self::IMAGE,
        self::LINK
    ];
}
