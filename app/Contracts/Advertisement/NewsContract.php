<?php

namespace App\Contracts\Advertisement;

interface NewsContract
{
    const TITLE = 'title';
    const BODY = 'body';
    const IMAGES = 'images';
    const LINK = 'link';

    const FILLABLE = [
        self::TITLE,
        self::IMAGES,
        self::BODY,
        self::LINK,
    ];
}
