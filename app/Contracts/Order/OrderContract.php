<?php

namespace App\Contracts\Order;

interface OrderContract
{
    const USER_ID = 'user_id';
    const COMMENTS = 'comments';
    const NAME = 'name';
    const ADDRESS = 'address';
    const PHONE = 'phone';
    const ORDER_STATUS_ID = 'order_status_id';
    const PRICE = 'price';

    const DEVICE_ID = 'device_id';


    const FILLABLE = [
        self::NAME,
        self::PRICE,
        self::ADDRESS,
        self::COMMENTS,
        self::USER_ID,
        self::PHONE,
        self::ORDER_STATUS_ID,
        self::DEVICE_ID,
    ];
}
