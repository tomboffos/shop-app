<?php

namespace App\Contracts\Order;

interface OrderItemContract
{
    const PRODUCT_ID = 'product_id';
    const PRICE = 'price';
    const QUANTITY = 'quantity';
    const ORDER_ID = 'order_id';


    const FILLABLE = [
        self::PRICE,
        self::PRODUCT_ID,
        self::QUANTITY,
        self::ORDER_ID
    ];
}
