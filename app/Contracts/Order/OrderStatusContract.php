<?php

namespace App\Contracts\Order;

interface OrderStatusContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME,
    ];
}
