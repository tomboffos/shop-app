<?php

namespace App\Contracts\Profile;

interface DeviceContract
{
    const TOKEN = 'token';
    const USER_ID = 'user_id';

    const FILLABLE = [
        self::TOKEN,
        self::USER_ID,
    ];
}
