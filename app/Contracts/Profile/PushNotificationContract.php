<?php

namespace App\Contracts\Profile;

interface PushNotificationContract
{
    const DEVICES = 'devices';
    const TITLE = 'text';
    const SUBTITLE = 'subtitle';

    const TYPE = 'type';

    const TYPE_VALUES = ['all', 'separate'];


    const FILLABLE = [
        self::DEVICES,
        self::TITLE,
        self::SUBTITLE
    ];
}
