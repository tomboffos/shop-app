<?php

namespace App\Contracts\Profile;

interface RoleContract
{
    const NAME = 'name';


    const FILLABLE = [
        self::NAME,
    ];


    const DATA = [
        [
            'id' => 1,
            self::NAME => 'user'
        ],
        [
            'id' => 2,
            self::NAME => 'admin'
        ]
    ];
}
