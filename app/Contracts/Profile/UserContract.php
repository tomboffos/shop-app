<?php

namespace App\Contracts\Profile;

interface UserContract
{
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';

    const ROLE_ID = 'role_id';


    const FILLABLE = [
        self::NAME,
        self::PASSWORD,
        self::EMAIL,
        self::ROLE_ID,
    ];
}
