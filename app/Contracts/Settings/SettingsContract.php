<?php

namespace App\Contracts\Settings;

interface SettingsContract
{
    const NAME = 'name';
    const VALUE = 'value';


    const FILLABLE = [
        self::NAME,
        self::VALUE,
    ];


    const PHONE_SETTINGS = [
        [
            self::NAME => 'primary_color_light',
            self::VALUE => '#ffffff'
        ],
        [
            self::NAME => 'primary_color_dark',
            self::VALUE => '#000000'
        ],
        [
            self::NAME => 'secondary_color_light',
            self::VALUE => '#ffffff'
        ],
        [
            self::NAME => 'secondary_color_dark',
            self::VALUE => '#000000'
        ],
        [
            self::NAME => 'title',
            self::VALUE => '',
        ],
        [
            self::NAME => 'logo',
            self::VALUE => ''
        ],
    ];
}
