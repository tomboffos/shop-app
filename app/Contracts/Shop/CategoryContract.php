<?php

namespace App\Contracts\Shop;

interface CategoryContract
{
    const NAME = 'name';
    const IMAGE = 'image';
    CONST CATEGORY_ID = 'category_id';


    const FILLABLE = [
        self::NAME,
        self::CATEGORY_ID,
        self::IMAGE
    ];
}
