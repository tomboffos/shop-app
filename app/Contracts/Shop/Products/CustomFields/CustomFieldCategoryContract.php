<?php

namespace App\Contracts\Shop\Products\CustomFields;

interface CustomFieldCategoryContract
{
    const CUSTOM_FIELD_ID = 'custom_field_id';
    const CATEGORY_ID = 'category_id';
    const IS_FILTER = 'is_filter';

    const FILLABLE = [
        self::CATEGORY_ID,
        self::CUSTOM_FIELD_ID,
        self::IS_FILTER,
    ];
}
