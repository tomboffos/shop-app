<?php

namespace App\Contracts\Shop\Products\CustomFields;

interface CustomFieldContract
{
    const NAME = 'name';
    const VALUES = 'values';
    const TYPE = 'type';

    const ADD_TO_FILTER = 'add_to_filter';

    const FILLABLE = [
        self::NAME,
        self::VALUES,
        self::TYPE,
        self::ADD_TO_FILTER,
    ];
}
