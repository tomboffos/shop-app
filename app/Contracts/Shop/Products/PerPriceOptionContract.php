<?php

namespace App\Contracts\Shop\Products;

interface PerPriceOptionContract
{
    const NAME = 'name';


    const FILLABLE = [
        self::NAME
    ];
}
