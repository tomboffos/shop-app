<?php

namespace App\Contracts\Shop\Products;

interface ProductCategoryContract
{
    const PRODUCT_ID = 'product_id';
    const CATEGORY_ID = 'category_id';

    const FILLABLE = [
      self::CATEGORY_ID,
      self::PRODUCT_ID
    ];
}
