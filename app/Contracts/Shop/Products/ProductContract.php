<?php

namespace App\Contracts\Shop\Products;

interface ProductContract
{
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PRICE = 'price';
    const PER_PRICE_OPTION_ID = 'per_price_option_id';
    const CUSTOM_FIELDS = 'custom_fields';
    const CATEGORY_ID = 'category_id';

    const ACTIVE = 'active';



    const FILLABLE = [
        self::CUSTOM_FIELDS,
        self::PER_PRICE_OPTION_ID,
        self::NAME,
        self::DESCRIPTION,
        self::PRICE,
        self::CATEGORY_ID,
        self::ACTIVE,
    ];

}
