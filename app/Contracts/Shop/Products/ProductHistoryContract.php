<?php

namespace App\Contracts\Shop\Products;

interface ProductHistoryContract
{
    const DEVICE_ID = 'device_id';
    const PRODUCT_ID = 'product_id';

    const FILLABLE = [
        self::DEVICE_ID,
        self::PRODUCT_ID
    ];
}
