<?php

namespace App\Contracts\Shop\Products;

interface ProductMediaContract
{
    const PRODUCT_ID = 'product_id';
    const MEDIA = 'image';
    const TYPE = 'type';

    const TYPE_VALUES = ['image', 'video'];

    const FILLABLE = [
        self::MEDIA,
        self::PRODUCT_ID,
        self::TYPE,
    ];

}
