<?php

namespace App\Contracts\Shop\RecommendationBloc;

interface RecommendationCategoryContract
{
    const CATEGORY_ID = 'category_id';
    const RECOMMENDATION_ID = 'recommendation_id';
    const ORDER = 'order';

    const FILLABLE = [
        self::CATEGORY_ID,
        self::RECOMMENDATION_ID,
        self::ORDER,
    ];
}
