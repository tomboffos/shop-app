<?php

namespace App\Contracts\Shop\RecommendationBloc;

interface RecommendationContract
{
    const TITLE = 'title';
    const ORDER = 'order';


    const FILLABLE = [
        self::TITLE,
        self::ORDER,
    ];
}
