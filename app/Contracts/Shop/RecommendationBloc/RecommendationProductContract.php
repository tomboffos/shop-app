<?php

namespace App\Contracts\Shop\RecommendationBloc;

interface RecommendationProductContract
{
    const PRODUCT_ID = 'product_id';
    const RECOMMENDATION_ID = 'recommendation_id';

    const ORDER = 'order';

    const FILLABLE = [
        self::PRODUCT_ID,
        self::RECOMMENDATION_ID,
        self::ORDER,
    ];
}
