<?php

namespace App\Contracts\Shop\Review;

interface ReviewContract
{
    const TEXT = 'text';
    const USER_ID = 'user_id';
    const RATING = 'rating';

    const PRODUCT_ID = 'product_id';


    const FILLABLE = [
        self::RATING,
        self::TEXT,
        self::USER_ID,
        self::PRODUCT_ID,
    ];
}
