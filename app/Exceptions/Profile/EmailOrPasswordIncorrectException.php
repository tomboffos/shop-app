<?php

namespace App\Exceptions\Profile;

class EmailOrPasswordIncorrectException extends \Exception
{
    protected $message = 'Email or password not found';

    protected $code = 404;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function render()
    {
        return response([
            'message' => $this->message,
        ], $this->code);
    }
}
