<?php

namespace App\Http\Controllers\Api\Advertisement;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisement\BannerResource;
use App\Service\Advertisement\Banner\BannerServiceInterface;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $service;
    public function __construct(
        BannerServiceInterface $service
    )
    {
        $this->service = $service;
    }


    public function index()
    {
        return BannerResource::collection($this->service->getBanners());
    }

}
