<?php

namespace App\Http\Controllers\Api\Advertisement;

use App\Http\Controllers\Controller;
use App\Http\Resources\Advertisement\NewsResource;
use App\Service\Advertisement\News\NewsServiceInterface;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    private $service;
    public function __construct(
        NewsServiceInterface $service
    )
    {
        $this->service = $service;
    }

    public function index()
    {
        return NewsResource::collection($this->service->getNews());
    }

}
