<?php

namespace App\Http\Controllers\Api\Order;

use App\Http\Controllers\Controller;
use App\Http\Requests\Order\OrderCreateRequest;
use App\Http\Requests\Order\OrderGetRequest;
use App\Http\Requests\Order\OrderServiceInterface;
use App\Http\Resources\Order\OrderResource;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $orderService;
    public function __construct(
        OrderServiceInterface $orderService
    )
    {
        $this->orderService = $orderService;
    }

    public function store(OrderCreateRequest $request)
    {
        return new OrderResource($this->orderService->create($request));
    }


    public function index(OrderGetRequest $orderGetRequest)
    {
        return OrderResource::collection($this->orderService->get($orderGetRequest));
    }

    public function getAdmin()
    {
        return OrderResource::collection($this->orderService->getAdmin());
    }
}
