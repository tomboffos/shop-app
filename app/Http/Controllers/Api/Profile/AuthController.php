<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\LoginRequest;
use App\Http\Requests\Profile\RegisterRequest;
use App\Http\Resources\Profile\AuthResource;
use App\Service\Profile\Auth\AuthServiceInterface;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    private $authService;

    //
    public function __construct(
        AuthServiceInterface $authService
    )
    {
        $this->authService = $authService;
    }


    public function login(LoginRequest $loginRequest): AuthResource
    {
        return new AuthResource($this->authService->login($loginRequest));
    }

    public function register(RegisterRequest $registerRequest): AuthResource
    {
        return new AuthResource($this->authService->register($registerRequest));
    }
}
