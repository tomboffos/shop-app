<?php

namespace App\Http\Controllers\Api\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\DeviceCreateRequest;
use App\Http\Requests\SendMessageRequest;
use App\Http\Resources\Profile\DeviceResource;
use App\Service\Profile\Device\DeviceServiceInterface;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    private $deviceService;

    public function __construct(
        DeviceServiceInterface $deviceService
    )
    {
        $this->deviceService = $deviceService;
    }


    public function create(DeviceCreateRequest $request)
    {
        return new DeviceResource($this->deviceService->addDevice($request));
    }


    public function send(SendMessageRequest $request)
    {
        return $this->deviceService->sendNotification($request);
    }
}
