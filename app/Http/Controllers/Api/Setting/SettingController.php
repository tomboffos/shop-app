<?php

namespace App\Http\Controllers\Api\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingUpdateRequest;
use App\Models\Setting;
use App\Service\Setting\SettingServiceInterface;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    private $settingService;

    public function __construct(
        SettingServiceInterface $service
    )
    {
        $this->settingService = $service;
    }

    public function index()
    {
        return $this->settingService->get();
    }

    public function update(SettingUpdateRequest $request)
    {
        return $this->settingService->update($request);
    }
}
