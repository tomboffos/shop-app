<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomFieldStoreRequest;
use App\Http\Resources\Shop\CustomFieldResource;
use App\Models\Category;
use App\Service\Shop\CustomField\CustomFieldServiceInterface;
use Illuminate\Http\Request;

class CustomFieldController extends Controller
{
    private $customFieldService;

    public function __construct(
        CustomFieldServiceInterface $customFieldService
    )
    {
        $this->customFieldService = $customFieldService;
    }

    public function getCategories(Category $category)
    {
        return CustomFieldResource::collection($this->customFieldService->getFilters($category));
    }

    public function getAdmin(Request $request)
    {
        return CustomFieldResource::collection($this->customFieldService->getAdmin($request));
    }

    public function store(CustomFieldStoreRequest $request)
    {
        return new CustomFieldResource($this->customFieldService->store($request));
    }


    public function update(CustomFieldStoreRequest $request, int $id)
    {
        return new CustomFieldResource($this->customFieldService->update($request, $id));
    }


    public function delete(int $id)
    {
        return $this->customFieldService->delete($id);
    }
}
