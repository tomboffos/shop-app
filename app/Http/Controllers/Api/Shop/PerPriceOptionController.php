<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\PerPriceOptionStoreRequest;
use App\Http\Requests\Shop\PerPriceOptionUpdateRequest;
use App\Http\Resources\Shop\PerPriceOptionResource;
use App\Service\Shop\PerPriceOption\PerPriceOptionService;
use Illuminate\Http\JsonResponse;

/**
 * @OA\Tag(
 *     name="PerPriceOptions",
 *     description="API Endpoints of Per Price Options"
 * )
 */
class PerPriceOptionController extends Controller
{
    private $perPriceOptionService;

    public function __construct(PerPriceOptionService $perPriceOptionService)
    {
        $this->perPriceOptionService = $perPriceOptionService;
    }

    /**
     * @OA\Get(
     *     path="/api/per-price-options",
     *     tags={"PerPriceOptions"},
     *     summary="Get list of per price options",
     *     description="Returns list of per price options",
     *     @OA\Response(response=200, description="successful operation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function index()
    {
        return PerPriceOptionResource::collection($this->perPriceOptionService->getPerPriceOptions());
    }

    /**
     * @OA\Post(
     *     path="/api/per-price-options",
     *     tags={"PerPriceOptions"},
     *     summary="Create a new per price option",
     *     description="Stores a new per price option",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/PerPriceOptionStoreRequest")
     *     ),
     *     @OA\Response(response=201, description="successful operation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function store(PerPriceOptionStoreRequest $request)
    {
        $perPriceOption = $this->perPriceOptionService->createPerPriceOption($request->validated());
        return response()->json(new PerPriceOptionResource($perPriceOption), 201);
    }

    /**
     * @OA\Get(
     *     path="/api/per-price-options/{id}",
     *     tags={"PerPriceOptions"},
     *     summary="Get a single per price option",
     *     description="Returns a single per price option by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="Per Price Option ID"
     *     ),
     *     @OA\Response(response=200, description="successful operation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function show($id)
    {
        $perPriceOption = $this->perPriceOptionService->getPerPriceOptionById($id);
        return response()->json(new PerPriceOptionResource($perPriceOption), 200);
    }

    /**
     * @OA\Put(
     *     path="/api/per-price-options/{id}",
     *     tags={"PerPriceOptions"},
     *     summary="Update a per price option",
     *     description="Updates an existing per price option by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="Per Price Option ID"
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/PerPriceOptionUpdateRequest")
     *     ),
     *     @OA\Response(response=200, description="successful operation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function update(PerPriceOptionUpdateRequest $request, $id)
    {
        $perPriceOption = $this->perPriceOptionService->updatePerPriceOption($id, $request->validated());
        return response()->json(new PerPriceOptionResource($perPriceOption), 200);
    }

    /**
     * @OA\Delete(
     *     path="/api/per-price-options/{id}",
     *     tags={"PerPriceOptions"},
     *     summary="Delete a per price option",
     *     description="Deletes a per price option by ID",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="Per Price Option ID"
     *     ),
     *     @OA\Response(response=204, description="successful operation"),
     *     @OA\Response(response=401, description="Unauthorized"),
     *     @OA\Response(response=400, description="Bad Request"),
     *     @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function destroy($id)
    {
        $this->perPriceOptionService->deletePerPriceOption($id);
        return response()->json(null, 204);
    }
}
