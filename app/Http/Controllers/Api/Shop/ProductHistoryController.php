<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\ProductHistoryAddRequest;
use App\Http\Resources\Shop\ProductResource;
use App\Models\Device;
use App\Service\Shop\History\ProductHistoryServiceInterface;
use Illuminate\Http\Request;

class ProductHistoryController extends Controller
{
    private $productHistoryService;

    public function __construct(
        ProductHistoryServiceInterface $historyService
    )
    {
        $this->productHistoryService = $historyService;
    }


    public function index(Device $device)
    {
        return ProductResource::collection($device->products()->paginate(10));
    }

    public function store(ProductHistoryAddRequest $request)
    {

        return $this->productHistoryService->addHistory($request);
    }

}
