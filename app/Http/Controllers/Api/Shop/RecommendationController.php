<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\RecommendationStoreRequest;
use App\Http\Resources\Shop\RecommendationResource;
use App\Service\Shop\Recommendation\RecommendationServiceInterface;
use Illuminate\Http\Request;

class RecommendationController extends Controller
{
    private $recommendationService;

    public function __construct(RecommendationServiceInterface  $service)
    {
        $this->recommendationService = $service;
    }

    public function index()
    {
        return RecommendationResource::collection($this->recommendationService->getRecommendations());
    }


    public function store(RecommendationStoreRequest $request)
    {
        return new RecommendationResource($this->recommendationService->store($request));
    }

    public function update(RecommendationStoreRequest $request, int $id)
    {
        return new RecommendationResource($this->recommendationService->update($id, $request));
    }


    public function delete($id)
    {
        return $this->recommendationService->delete($id);
    }
}
