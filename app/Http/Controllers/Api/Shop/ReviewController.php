<?php

namespace App\Http\Controllers\Api\Shop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Shop\ReviewCreateRequest;
use App\Http\Resources\Shop\ReviewResource;
use App\Models\Product;
use App\Service\Shop\Review\ReviewServiceInterface;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    private $reviewService;

    public function __construct(ReviewServiceInterface $service)
    {
        $this->reviewService = $service;
    }

    public function index(Product $product)
    {
        return ReviewResource::collection($this->reviewService->getReviewsFromProduct($product));
    }

    public function store(ReviewCreateRequest $request)
    {
        return ReviewResource::collection($this->reviewService->addReview($request));
    }
}
