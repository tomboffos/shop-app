<?php

namespace App\Http\Requests;

use App\Contracts\Shop\Products\CustomFields\CustomFieldContract;
use Illuminate\Foundation\Http\FormRequest;

class CustomFieldStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CustomFieldContract::TYPE => 'required',
            CustomFieldContract::NAME => 'required',
            CustomFieldContract::VALUES => 'nullable',
            'categories' => 'nullable'
        ];
    }
}
