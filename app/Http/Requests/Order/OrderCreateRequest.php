<?php

namespace App\Http\Requests\Order;

use App\Contracts\Order\OrderContract;
use App\Contracts\Order\OrderItemContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $user_id
 * @property array $products
 * @property $address
 * @property $name
 * @property $phone
 * @property $comments
 * @property $device_id
 *
 */
class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            OrderContract::DEVICE_ID => ['nullable'],
            OrderContract::USER_ID => ['nullable'],
            OrderContract::NAME => ['nullable'],
            OrderContract::ADDRESS => ['nullable'],
            OrderContract::PHONE => ['nullable'],
            OrderContract::COMMENTS => ['nullable'],
            'products.*.'.OrderItemContract::PRODUCT_ID => ['required', 'exists:products,id'],
            'products.*.'.OrderItemContract::QUANTITY => ['required', 'numeric'],
        ];
    }
}
