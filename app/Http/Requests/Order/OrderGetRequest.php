<?php

namespace App\Http\Requests\Order;

use App\Contracts\Order\OrderContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $user_id
 * @property $device_id
 */
class OrderGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            OrderContract::DEVICE_ID => ['nullable'],
            OrderContract::USER_ID => ['nullable']
        ];
    }
}
