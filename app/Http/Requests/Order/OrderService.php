<?php

namespace App\Http\Requests\Order;

use App\Contracts\Order\OrderContract;
use App\Contracts\Order\OrderItemContract;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;

class OrderService implements OrderServiceInterface
{

    private function calculatePrice(array $products): float
    {
        $overallPrice = 0;

        foreach ($products as $product) {
            $productModel = Product::query()->find($product['product_id']);

            $overallPrice += $productModel->price * $product['quantity'];

        }

        return $overallPrice;
    }

    private function createItems(array $products, Order $order)
    {
        foreach ($products as $product) {

            $productModel = Product::query()->find($product['product_id']);

            OrderItem::query()->create([
                OrderItemContract::ORDER_ID => $order->id,
                OrderItemContract::PRICE => $productModel->price * $product['quantity'],
                OrderItemContract::QUANTITY => $product['quantity'],
                OrderItemContract::PRODUCT_ID => $productModel->id,
            ]);
        }
    }

    public function create(OrderCreateRequest $request)
    {
        $price = $this->calculatePrice($request->products);

        $order = Order::query()->create([
            OrderContract::ORDER_STATUS_ID => 1,
            OrderContract::COMMENTS => $request->comments,
            OrderContract::NAME => $request->name,
            OrderContract::PHONE => $request->phone,
            OrderContract::USER_ID => $request->user_id,
            OrderContract::PRICE => $price,
            OrderContract::DEVICE_ID => $request->device_id
        ]);

        $this->createItems($request->products, $order);

        return $order;
    }

    public function get(OrderGetRequest $request)
    {
        return Order::query()
            ->where(function ($query) use ($request) {
                if ($request->has(OrderContract::DEVICE_ID)) {
                    $query->where(OrderContract::DEVICE_ID, $request->device_id);
                }

                if ($request->has(OrderContract::USER_ID)) {
                    $query->where(OrderContract::USER_ID, $request->user_id);
                }
            })
            ->orderBy('id', 'desc')
            ->get();
    }


    public function getAdmin()
    {
        return Order::query()

            ->orderBy('id', 'desc')
            ->get();
    }
}
