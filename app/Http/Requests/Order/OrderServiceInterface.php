<?php

namespace App\Http\Requests\Order;

interface OrderServiceInterface
{
    public function create(OrderCreateRequest $request);

    public function get(OrderGetRequest $request);
}
