<?php

namespace App\Http\Requests\Profile;

use App\Contracts\Profile\DeviceContract;
use App\Contracts\Profile\UserContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $token
 */
class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            UserContract::EMAIL => ['required', 'email'],
            UserContract::PASSWORD => ['required', 'min:8'],
            UserContract::NAME => ['required'],
            DeviceContract::TOKEN => ['required'],
        ];
    }
}
