<?php

namespace App\Http\Requests;

use App\Contracts\Shop\RecommendationBloc\RecommendationContract;
use Illuminate\Foundation\Http\FormRequest;

class RecommendationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            RecommendationContract::TITLE => 'required',
            RecommendationContract::ORDER => 'nullable',
            'categories' => 'nullable',
            'products' => 'nullable',
        ];
    }
}
