<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="CategoryStoreRequest",
 *     type="object",
 *     required={"name", "image", "category_id"},
 *     @OA\Property(property="name", type="string", example="Sample Category"),
 *     @OA\Property(property="image", type="string", format="binary"),
 *     @OA\Property(property="category_id", type="integer", example=1)
 * )
 */
class CategoryStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'image' => 'required', // Adjust validation if storing image differently
            'category_id' => 'nullable',
            'products' => 'nullable',
            'fields' => 'nullable',
        ];
    }
}
