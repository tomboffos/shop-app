<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="CategoryUpdateRequest",
 *     type="object",
 *     @OA\Property(property="name", type="string", example="Sample Category"),
 *     @OA\Property(property="image", type="string", format="binary"),
 *     @OA\Property(property="category_id", type="integer", example=1)
 * )
 */
class CategoryUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:255',
            'image' => 'sometimes|required|string', // Adjust validation if storing image differently
            'category_id' => 'sometimes|required|integer',
        ];
    }
}
