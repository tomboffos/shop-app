<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="PerPriceOptionUpdateRequest",
 *     type="object",
 *     @OA\Property(property="name", type="string", example="Sample Per Price Option")
 * )
 */
class PerPriceOptionUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'sometimes|required|string|max:255',
            // Add other validation rules as necessary
        ];
    }
}
