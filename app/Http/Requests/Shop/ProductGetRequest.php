<?php

namespace App\Http\Requests\Shop;

use App\Contracts\Shop\Products\ProductContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $price_from
 * @property $price_to
 * @property $category_id
 * @property $search
 * @property $filters
 */
class ProductGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price_from' => ['nullable', 'numeric'],
            'price_to' => ['nullable', 'numeric'],
            ProductContract::CATEGORY_ID => ['nullable',],
            'search' => ['nullable'],
            'filters' => ['nullable'],
        ];
    }
}
