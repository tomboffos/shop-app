<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *     schema="ProductStoreRequest",
 *     type="object",
 *     required={"name", "description", "price"},
 *     @OA\Property(property="name", type="string", example="Sample Product"),
 *     @OA\Property(property="description", type="string", example="This is a sample product."),
 *     @OA\Property(property="price", type="number", format="float", example=19.99)
 * )
 */
class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'per_price_option_id' => 'required',
            'active' => 'nullable',
            'custom_fields' => 'nullable',
            'categories' => 'nullable',
            'medias' => 'nullable'
            // add other validation rules as necessary
        ];
    }
}
