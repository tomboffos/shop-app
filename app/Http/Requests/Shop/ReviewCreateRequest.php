<?php

namespace App\Http\Requests\Shop;

use App\Contracts\Shop\Review\ReviewContract;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $product_id
 * @property $rating
 * @property $text
 */
class ReviewCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ReviewContract::PRODUCT_ID => ['required'],
            ReviewContract::TEXT => ['nullable'],
            ReviewContract::RATING => ['nullable'],
        ];
    }
}
