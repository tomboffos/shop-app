<?php

namespace App\Http\Resources\Order;

use App\Contracts\Order\OrderItemContract;
use App\Http\Resources\Shop\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            OrderItemContract::PRICE => intval($this->price),
            OrderItemContract::QUANTITY => $this->quantity,
            'product' => new ProductResource($this->product),
            'id' => $this->id
        ];
    }
}
