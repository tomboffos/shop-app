<?php

namespace App\Http\Resources\Order;

use App\Contracts\Order\OrderContract;
use App\Contracts\Order\OrderItemContract;
use App\Http\Resources\Shop\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            OrderContract::PRICE => (int) $this->price,
            OrderContract::NAME => $this->name,
            OrderContract::PHONE => $this->phone,
            'status' => $this->status,
            'products' => OrderItemResource::collection($this->products),
            'user' => $this->user,
            OrderContract::COMMENTS => $this->comments,
            OrderContract::ADDRESS => $this->address,
            'id' => $this->id,
            'created_at' => $this->created_at
        ];
    }
}
