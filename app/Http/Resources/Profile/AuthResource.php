<?php

namespace App\Http\Resources\Profile;

use App\Contracts\Profile\DeviceContract;
use App\Contracts\Profile\UserContract;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{

    private $withoutToken;
    public function __construct($resource, $withoutToken = false)
    {
        $this->resource = $resource;
        $this->withoutToken = $withoutToken;
    }
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $resource = [
            'id' => $this->id,
            UserContract::NAME => $this->name,
            UserContract::EMAIL => $this->email,
            UserContract::ROLE_ID => $this->role_id
        ];

        if (!$this->withoutToken){
            $resource[DeviceContract::TOKEN] = $this->createToken(env('APP_NAME'))->plainTextToken;
        }

        return $resource;
    }
}
