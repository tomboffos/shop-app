<?php

namespace App\Http\Resources\Shop;

use App\Contracts\Shop\CategoryContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            CategoryContract::NAME => $this->name,
            CategoryContract::IMAGE => $this->image,
            'children' => count($this->children),
            'id' => $this->id,
            'fields' => CustomFieldResource::collection($this->customFields)
        ];
    }
}
