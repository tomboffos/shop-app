<?php

namespace App\Http\Resources\Shop;

use App\Contracts\Shop\Products\CustomFields\CustomFieldContract;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomFieldResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            CustomFieldContract::NAME => $this->name,
            CustomFieldContract::TYPE => $this->type,
            CustomFieldContract::VALUES => json_decode($this->values),
            'id' => $this->id,
            'categories' => $this->categories
        ];
    }
}
