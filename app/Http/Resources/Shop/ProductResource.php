<?php

namespace App\Http\Resources\Shop;

use App\Contracts\Shop\Products\ProductContract;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'categories' => CategoryResource::collection($this->categories),
            ProductContract::NAME => $this->name,
            ProductContract::DESCRIPTION => $this->description,
            ProductContract::PRICE => $this->price,
            ProductContract::ACTIVE => $this->active,
            'per_price' => $this->perPriceOption,
            ProductContract::CUSTOM_FIELDS => json_decode($this->custom_fields),
            'medias' => $this->medias,
            'id' => $this->id,
        ];
    }
}
