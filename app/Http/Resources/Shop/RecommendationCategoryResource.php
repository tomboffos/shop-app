<?php

namespace App\Http\Resources\Shop;

use App\Models\RecommendationCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class RecommendationCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return new CategoryResource($this->category);
    }
}
