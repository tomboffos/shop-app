<?php

namespace App\Http\Resources\Shop;

use App\Contracts\Shop\RecommendationBloc\RecommendationCategoryContract;
use Illuminate\Http\Resources\Json\JsonResource;

class RecommendationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'order' => $this->order,
            'products' => RecommendationProductResource::collection($this->products()),
            'categories' => RecommendationCategoryResource::collection($this->categories()),
        ];
    }
}
