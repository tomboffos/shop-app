<?php

namespace App\Http\Resources\Shop;

use App\Contracts\Shop\Review\ReviewContract;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            ReviewContract::TEXT => $this->text,
            ReviewContract::RATING => $this->rating,
            'user' => $this->user
        ];
    }
}
