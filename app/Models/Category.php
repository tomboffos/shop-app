<?php

namespace App\Models;

use App\Service\StoreService;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Contracts\Shop\CategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property $id
 * @property $name
 * @property $image
 * @property $categoryId
 */
class Category extends Model
{
    use CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        CategoryContract::NAME,
        CategoryContract::IMAGE,
        CategoryContract::CATEGORY_ID
    ];


    public function children(): HasMany
    {
        return $this->hasMany(Category::class);
    }

    public function setImageAttribute($value)
    {
        $this->attributes[CategoryContract::IMAGE] = is_string($value) ? $value : StoreService::storeFile($value);
    }


    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id');
    }


    public function customFields()
    {
        return $this->belongsToMany(CustomField::class,'custom_field_categories');
    }

}
