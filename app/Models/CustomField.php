<?php

namespace App\Models;

use App\Contracts\Shop\Products\CustomFields\CustomFieldContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomField extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = CustomFieldContract::FILLABLE;


    public function setValuesAttribute($value)
    {
        $this->attributes[CustomFieldContract::VALUES] = json_encode($value);
    }
    public function customFieldCategories(): HasMany
    {
        return $this->hasMany(CustomFieldCategory::class, 'custom_field_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'custom_field_categories');
    }
}
