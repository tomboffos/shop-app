<?php

namespace App\Models;

use App\Contracts\Shop\Products\CustomFields\CustomFieldCategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomFieldCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = CustomFieldCategoryContract::FILLABLE;


    public function categories(): HasMany
    {
        return $this->hasMany(Category::class,'category_id');
    }
}
