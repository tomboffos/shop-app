<?php

namespace App\Models;

use App\Contracts\Profile\DeviceContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = DeviceContract::FILLABLE;

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_histories');
    }
}
