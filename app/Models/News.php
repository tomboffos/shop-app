<?php

namespace App\Models;

use App\Contracts\Advertisement\NewsContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = NewsContract::FILLABLE;


    public function getImagesAttribute()
    {
        return json_decode($this->attributes[NewsContract::IMAGES]);
    }
}
