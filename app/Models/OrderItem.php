<?php

namespace App\Models;

use App\Contracts\Order\OrderItemContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = OrderItemContract::FILLABLE;

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
