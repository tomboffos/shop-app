<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Contracts\Shop\Products\PerPriceOptionContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerPriceOption extends Model
{
    use CrudTrait;
    use HasFactory, SoftDeletes;


    protected $fillable = PerPriceOptionContract::FILLABLE;
    /**
     * Get the products associated with the per price option.
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'per_price_option_id');
    }
}
