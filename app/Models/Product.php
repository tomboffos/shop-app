<?php

namespace App\Models;

use App\Contracts\Shop\Products\CustomFields\CustomFieldContract;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Contracts\Shop\Products\ProductContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property $name
 * @property $description
 * @property $price
 * @property $per_price_option_id
 * @property $category_id
 * @property $active
 */
class Product extends Model
{
    use CrudTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = ProductContract::FILLABLE;


    /**
     * The categories that belong to the product.
     */
    /**
     * The categories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function setCustomFieldsAttribute($value)
    {
        $this->attributes[ProductContract::CUSTOM_FIELDS] = json_encode($value);
    }


    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function medias(): HasMany
    {
        return $this->hasMany(ProductMedia::class);
    }

    public function perPriceOption()
    {
        return $this->belongsTo(PerPriceOption::class, 'per_price_option_id');
    }
}
