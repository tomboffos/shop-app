<?php

namespace App\Models;

use App\Contracts\Shop\Products\ProductHistoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductHistory extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ProductHistoryContract::FILLABLE;
}
