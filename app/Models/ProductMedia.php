<?php

namespace App\Models;

use App\Contracts\Shop\Products\ProductMediaContract;
use App\Service\StoreService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductMedia extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ProductMediaContract::FILLABLE;

    public function setImageAttribute($value)
    {
        $this->attributes[ProductMediaContract::MEDIA] = is_string($value) ? $value : StoreService::storeFile($value);
    }
}
