<?php

namespace App\Models;

use App\Contracts\Profile\PushNotificationContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotification extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = PushNotificationContract::FILLABLE;
}
