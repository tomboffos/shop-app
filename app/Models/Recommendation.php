<?php

namespace App\Models;

use App\Contracts\Shop\RecommendationBloc\RecommendationCategoryContract;
use App\Contracts\Shop\RecommendationBloc\RecommendationContract;
use App\Contracts\Shop\RecommendationBloc\RecommendationProductContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property $title
 * @property $order
 */
class Recommendation extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = RecommendationContract::FILLABLE;


    public function products()
    {

        return RecommendationProduct::query()->where(RecommendationProductContract::RECOMMENDATION_ID, $this->attributes['id'])
            ->orderBy(RecommendationProductContract::ORDER, 'asc')->get();
    }


    public function categories()
    {

        return RecommendationCategory::query()->where(RecommendationCategoryContract::RECOMMENDATION_ID, $this->attributes['id'])
            ->orderBy(RecommendationCategoryContract::ORDER, 'asc')->get();
    }
}
