<?php

namespace App\Models;

use App\Contracts\Shop\RecommendationBloc\RecommendationCategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecommendationCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = RecommendationCategoryContract::FILLABLE;


    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
