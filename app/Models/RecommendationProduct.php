<?php

namespace App\Models;

use App\Contracts\Shop\RecommendationBloc\RecommendationProductContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecommendationProduct extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = RecommendationProductContract::FILLABLE;

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
