<?php

namespace App\Models;

use App\Contracts\Shop\Review\ReviewContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Review extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ReviewContract::FILLABLE;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
