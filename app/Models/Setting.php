<?php

namespace App\Models;

use App\Contracts\Settings\SettingsContract;
use App\Service\StoreService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property $name
 * @property $value
 */
class Setting extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = SettingsContract::FILLABLE;


    public function setValueAttribute($value)
    {
        $this->attributes[SettingsContract::VALUE] = is_string($value) ? $value : StoreService::storeFile($value);
    }
}
