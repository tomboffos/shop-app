<?php

namespace App\Providers;

use App\Http\Requests\Order\OrderService;
use App\Http\Requests\Order\OrderServiceInterface;
use App\Service\Advertisement\Banner\BannerService;
use App\Service\Advertisement\Banner\BannerServiceInterface;
use App\Service\Advertisement\News\NewsService;
use App\Service\Advertisement\News\NewsServiceInterface;
use App\Service\Profile\Auth\AuthService;
use App\Service\Profile\Auth\AuthServiceInterface;
use App\Service\Profile\Device\DeviceService;
use App\Service\Profile\Device\DeviceServiceInterface;
use App\Service\Setting\SettingService;
use App\Service\Setting\SettingServiceInterface;
use App\Service\Shop\Category\CategoryService;
use App\Service\Shop\Category\CategoryServiceInterface;
use App\Service\Shop\CustomField\CustomFieldService;
use App\Service\Shop\CustomField\CustomFieldServiceInterface;
use App\Service\Shop\History\ProductHistoryService;
use App\Service\Shop\History\ProductHistoryServiceInterface;
use App\Service\Shop\Product\ProductService;
use App\Service\Shop\Product\ProductServiceInterface;
use App\Service\Shop\Recommendation\RecommendationService;
use App\Service\Shop\Recommendation\RecommendationServiceInterface;
use App\Service\Shop\Review\ReviewService;
use App\Service\Shop\Review\ReviewServiceInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
        $this->app->bind(CategoryServiceInterface::class, CategoryService::class);
        $this->app->bind(ProductServiceInterface::class, ProductService::class);
        $this->app->bind(DeviceServiceInterface::class, DeviceService::class);
        $this->app->bind(ProductHistoryServiceInterface::class, ProductHistoryService::class);
        $this->app->bind(ReviewServiceInterface::class, ReviewService::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
        $this->app->bind(CustomFieldServiceInterface::class, CustomFieldService::class);
        $this->app->bind(BannerServiceInterface::class, BannerService::class);
        $this->app->bind(NewsServiceInterface::class, NewsService::class);
        $this->app->bind(SettingServiceInterface::class, SettingService::class);
        $this->app->bind(RecommendationServiceInterface::class, RecommendationService::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
