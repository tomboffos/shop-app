<?php

namespace App\Service\Advertisement\Banner;

use App\Models\Banner;

class BannerService implements BannerServiceInterface
{
    public function getBanners()
    {
        return Banner::query()->orderBy('id','desc')->paginate(10);
    }
}
