<?php

namespace App\Service\Advertisement\Banner;

interface BannerServiceInterface
{
    public function getBanners();
}
