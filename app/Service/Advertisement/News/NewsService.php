<?php

namespace App\Service\Advertisement\News;

use App\Models\News;

class NewsService implements NewsServiceInterface
{
    public function getNews()
    {
        return News::query()->orderBy('id', 'desc')->paginate(10);
    }
}
