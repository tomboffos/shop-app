<?php

namespace App\Service\Advertisement\News;

interface NewsServiceInterface
{
    public function getNews();
}
