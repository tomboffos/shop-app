<?php

namespace App\Service\Profile\Auth;

use App\Contracts\Profile\DeviceContract;
use App\Contracts\Profile\UserContract;
use App\Exceptions\Profile\EmailOrPasswordIncorrectException;
use App\Http\Requests\Profile\LoginRequest;
use App\Http\Requests\Profile\RegisterRequest;
use App\Models\Device;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService implements AuthServiceInterface
{
    public function register(RegisterRequest $registerRequest): ?User
    {
        $user = User::query()->create($registerRequest->except(DeviceContract::TOKEN));

        Device::query()->create([
            DeviceContract::USER_ID => $user->id,
            DeviceContract::TOKEN => $registerRequest->token
        ]);

        return $user;
    }

    public function login(LoginRequest $loginRequest): ?User
    {
        try {
            $user = User::query()->firstWhere(UserContract::EMAIL, $loginRequest->email);

            if (!Hash::check($loginRequest->password, $user->password))
                throw new EmailOrPasswordIncorrectException();

            Device::query()->firstOrCreate([
                DeviceContract::TOKEN => $loginRequest->token
            ], [
                DeviceContract::USER_ID => $user->id,
            ]);

            return $user;
        } catch (\Exception $exception) {
            throw new EmailOrPasswordIncorrectException();
        }
    }
}
