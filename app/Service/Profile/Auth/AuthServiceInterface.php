<?php

namespace App\Service\Profile\Auth;

use App\Http\Requests\Profile\LoginRequest;
use App\Http\Requests\Profile\RegisterRequest;
use App\Models\User;

interface AuthServiceInterface
{

    public function register(RegisterRequest $registerRequest): ?User;


    public function login(LoginRequest $loginRequest): ?User;
}
