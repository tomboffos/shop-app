<?php

namespace App\Service\Profile\Device;

use App\Http\Requests\Profile\DeviceCreateRequest;
use App\Http\Requests\SendMessageRequest;
use App\Models\Device;
use Illuminate\Http\Request;
use Kreait\Laravel\Firebase\Facades\Firebase;

class DeviceService implements DeviceServiceInterface
{

    public function addDevice(DeviceCreateRequest $request): ?Device
    {
        return Device::query()->create($request->validated());
    }


    public function sendNotification(SendMessageRequest $request)
    {
        Firebase::messaging()->sendAll($request->title);

        return true;
    }
}
