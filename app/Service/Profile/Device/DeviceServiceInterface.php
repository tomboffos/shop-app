<?php

namespace App\Service\Profile\Device;

use App\Http\Requests\Profile\DeviceCreateRequest;

interface DeviceServiceInterface
{
    public function addDevice(DeviceCreateRequest $request);
}
