<?php

namespace App\Service\Setting;

use App\Contracts\Settings\SettingsContract;
use App\Http\Requests\SettingUpdateRequest;
use App\Models\Setting;

class SettingService implements SettingServiceInterface
{

    public function get()
    {
        $settings = Setting::query()->get();

        $settingsData = [];

        foreach ($settings as $setting){
            $settingsData[$setting->name] = $setting->value;
        }


        return $settingsData;
    }


    public function update(SettingUpdateRequest $request)
    {
        Setting::query()->where(SettingsContract::NAME, $request->name)->first()->update([
            SettingsContract::NAME => $request->name,
            SettingsContract::VALUE => $request->value
        ]);


        return true;
    }
}
