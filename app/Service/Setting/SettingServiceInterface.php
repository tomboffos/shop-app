<?php

namespace App\Service\Setting;

interface SettingServiceInterface
{
    public function get();
}
