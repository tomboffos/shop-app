<?php

namespace App\Service\Shop\Category;

use App\Contracts\Shop\CategoryContract;
use App\Contracts\Shop\Products\CustomFields\CustomFieldCategoryContract;
use App\Contracts\Shop\Products\ProductCategoryContract;
use App\Http\Requests\Shop\CategoryStoreRequest;
use App\Models\Category;
use App\Models\CustomFieldCategory;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class CategoryService implements CategoryServiceInterface
{
    public function getCategories($categoryId = null)
    {
        return Category::query()->where(CategoryContract::CATEGORY_ID, $categoryId)->get();
    }


    public function getAdmin(Request $request)
    {
        return Category::query()->where(CategoryContract::NAME, 'iLIKE','%'.$request->search.'%')->get();
    }

    public function createCategory(CategoryStoreRequest $request)
    {
        $category = Category::create($request->except('products'));

        if ($request->has('products')) {
            foreach ($request->products as $item) {
                ProductCategory::create([
                    ProductCategoryContract::CATEGORY_ID => $category->id,
                    ProductCategoryContract::PRODUCT_ID => $item,
                ]);
            }
        }

        if ($request->has('fields')) {
            foreach ($request->fields as $item) {
                CustomFieldCategory::query()->create([
                    CustomFieldCategoryContract::CATEGORY_ID => $category->id,
                    CustomFieldCategoryContract::CUSTOM_FIELD_ID => $item,
                ]);
            }
        }
        return $category;
    }

    public function getCategoryById($id)
    {
        return Category::findOrFail($id);
    }

    public function updateCategory($id, CategoryStoreRequest $request)
    {
        $category = Category::findOrFail($id);
        $category->update($request->except('products'));

        if ($request->has('products')) {
            ProductCategory::query()->where(ProductCategoryContract::CATEGORY_ID, $id)->forceDelete();
            foreach ($request->products as $item) {
                ProductCategory::query()->create([
                    ProductCategoryContract::CATEGORY_ID => $id,
                    ProductCategoryContract::PRODUCT_ID => $item,
                ]);
            }
        }

        if ($request->has('fields')) {
            CustomFieldCategory::query()->where(CustomFieldCategoryContract::CATEGORY_ID, $id)->forceDelete();
            foreach ($request->fields as $item) {
                CustomFieldCategory::query()->create([
                    CustomFieldCategoryContract::CATEGORY_ID => $id,
                    CustomFieldCategoryContract::CUSTOM_FIELD_ID => $item,
                ]);
            }
        }

        return $category;
    }

    public function deleteCategory($id)
    {
        $category = Category::query()->findOrFail($id);

        ProductCategory::query()->where(ProductCategoryContract::CATEGORY_ID, $id)->forceDelete();
        $category->delete();

        return response()->json(['message' => 'Category deleted successfully']);
    }
}
