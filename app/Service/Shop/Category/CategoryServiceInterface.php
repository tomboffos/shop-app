<?php

namespace App\Service\Shop\Category;

use Illuminate\Database\Eloquent\Collection;

interface CategoryServiceInterface
{
    public function getCategories($categoryId = null);
}
