<?php

namespace App\Service\Shop\CustomField;

use App\Contracts\Shop\Products\CustomFields\CustomFieldCategoryContract;
use App\Contracts\Shop\Products\CustomFields\CustomFieldContract;
use App\Http\Requests\CustomFieldStoreRequest;
use App\Models\Category;
use App\Models\CustomField;
use App\Models\CustomFieldCategory;
use App\Service\Shop\Product\ProductServiceInterface;
use Illuminate\Http\Request;

class CustomFieldService implements CustomFieldServiceInterface
{
    private $productService;

    public function __construct(
        ProductServiceInterface $productService
    )
    {
        $this->productService = $productService;
    }

    public function getFilters(Category $category)
    {
        return CustomField::query()->whereHas('customFieldCategories', function ($query) use ($category) {
            $query->whereIn('custom_field_categories.category_id', array_unique($this->productService->getIdsOfChildCategories($category->id)));

            $query->where('custom_field_categories.is_filter', true);
        })->get();
    }


    public function getAdmin(Request $request)
    {
        return CustomField::query()->where(CustomFieldContract::NAME, 'iLIKE', '%'.$request->search.'%')->get();
    }

    public function store(CustomFieldStoreRequest $request)
    {
        $customField = CustomField::query()->create($request->except('categories'));


        if ($request->has('categories')) {
            foreach ($request->categories as $category) {
                CustomFieldCategory::query()->create([
                    CustomFieldCategoryContract::CUSTOM_FIELD_ID => $customField->id,
                    CustomFieldCategoryContract::CATEGORY_ID => $category,
                    CustomFieldCategoryContract::IS_FILTER => true
                ]);
            }
        }


        return $customField;
    }


    public function update(CustomFieldStoreRequest $request, int $id)
    {
        $customField = CustomField::query()->findOrFail($id);

        $customField->update($request->except('categories'));

        if ($request->has('categories')) {
            CustomFieldCategory::query()->where(CustomFieldCategoryContract::CUSTOM_FIELD_ID, $id)->forceDelete();
            foreach ($request->categories as $category) {
                CustomFieldCategory::query()->create([
                    CustomFieldCategoryContract::CUSTOM_FIELD_ID => $customField->id,
                    CustomFieldCategoryContract::CATEGORY_ID => $category,
                    CustomFieldCategoryContract::IS_FILTER => true
                ]);
            }
        }


        return $customField;
    }


    public function delete(int $id)
    {
        $customField = CustomField::query()->findOrFail($id);
        CustomFieldCategory::query()->where(CustomFieldCategoryContract::CUSTOM_FIELD_ID, $id)->forceDelete();
        $customField->delete();
        return response(['Deleted']);
    }


}
