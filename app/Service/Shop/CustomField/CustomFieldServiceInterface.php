<?php

namespace App\Service\Shop\CustomField;

use App\Models\Category;

interface CustomFieldServiceInterface
{
    public function getFilters(Category $category);

}
