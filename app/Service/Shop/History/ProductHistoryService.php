<?php

namespace App\Service\Shop\History;

use App\Http\Requests\Shop\ProductHistoryAddRequest;
use App\Models\Device;
use App\Models\ProductHistory;

class ProductHistoryService implements ProductHistoryServiceInterface
{

    public function addHistory(ProductHistoryAddRequest $request): ProductHistory
    {
        return ProductHistory::query()->create($request->validated());
    }

    public function getHistory(Device $device)
    {
        return $device->products;
    }
}
