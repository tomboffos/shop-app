<?php

namespace App\Service\Shop\History;

use App\Http\Requests\Shop\ProductHistoryAddRequest;
use App\Models\Device;

interface ProductHistoryServiceInterface
{
    public function addHistory(ProductHistoryAddRequest $request);

    public function getHistory(Device $device);
}
