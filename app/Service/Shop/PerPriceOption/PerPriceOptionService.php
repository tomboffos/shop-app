<?php

namespace App\Service\Shop\PerPriceOption;

use App\Models\PerPriceOption;

class PerPriceOptionService
{
    public function getPerPriceOptions()
    {
        return PerPriceOption::all();
    }

    public function createPerPriceOption(array $data)
    {
        return PerPriceOption::create($data);
    }

    public function getPerPriceOptionById($id)
    {
        return PerPriceOption::findOrFail($id);
    }

    public function updatePerPriceOption($id, array $data)
    {
        $perPriceOption = PerPriceOption::findOrFail($id);
        $perPriceOption->update($data);
        return $perPriceOption;
    }

    public function deletePerPriceOption($id)
    {
        $perPriceOption = PerPriceOption::findOrFail($id);
        $perPriceOption->delete();
    }
}
