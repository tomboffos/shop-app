<?php

namespace App\Service\Shop\Product;

use App\Contracts\Shop\CategoryContract;
use App\Contracts\Shop\Products\ProductCategoryContract;
use App\Contracts\Shop\Products\ProductContract;
use App\Contracts\Shop\Products\ProductMediaContract;
use App\Http\Requests\Shop\ProductGetRequest;
use App\Http\Requests\Shop\ProductStoreRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductMedia;

class ProductService implements ProductServiceInterface
{
    public function getProducts(ProductGetRequest $request)
    {
        return Product::query()->where(function ($query) use ($request) {
            if (isset($request->price_from)) {
                $query->where(ProductContract::PRICE, '>=', $request->price_from);
            }

            if (isset($request->price_to)) {
                $query->where(ProductContract::PRICE, '<=', $request->price_to);
            }

            if (isset($request->search)) {
                $query->where(function ($secondQuery) use ($request) {
                    $secondQuery
                        ->where(ProductContract::NAME, 'iLIKE', '%' . $request->search . '%')
                        ->orWhere(ProductContract::DESCRIPTION, 'iLIKE', '%' . $request->search . '%');
                });
            }

            if (isset($request->category_id)) {
                $query->whereHas('categories', function ($query) use ($request) {
                    $query->whereIn('categories.id', $this->getIdsOfChildCategories($request->category_id));
                });
            }

            if (isset($request->filters)) {
                foreach ($request->filters as $key => $filter) {
                    $query->where("custom_fields->$key", $filter);
                }
            }
        })->where(ProductContract::ACTIVE, true)->orderBy('id', 'desc')->get();


    }

    public function getIdsOfChildCategories(int $categoryId): array
    {
        $idsOfCategory = [$categoryId];

        $categories = Category::query()->where(CategoryContract::CATEGORY_ID, $categoryId)->get();

        foreach ($categories as $category) {
            $idsOfCategory[] = $category->id;
            $idsOfCategory = array_merge($idsOfCategory, $this->getIdsOfChildCategories($category->id));
        }

        return $idsOfCategory;
    }

    public function createProduct(ProductStoreRequest $request)
    {
        $product = Product::query()->create($request->except('medias', 'categories'));

        if ($request->has('categories')) {
            foreach ($request->categories as $item) {
                ProductCategory::query()->create([
                    ProductCategoryContract::CATEGORY_ID => $item,
                    ProductCategoryContract::PRODUCT_ID => $product->id,
                ]);
            }
        }

        if ($request->has('medias')) {
            foreach ($request->medias as $media) {
                ProductMedia::query()->create([
                    ProductMediaContract::MEDIA => $media,
                    ProductMediaContract::TYPE => 'image',
                    ProductMediaContract::PRODUCT_ID => $product->id
                ]);
            }
        }

        return $product;
    }

    public function getProductById($id)
    {
        return Product::findOrFail($id);
    }

    public function updateProduct($id, ProductStoreRequest $request)
    {
        $product = Product::query()->findOrFail($id);

        $product->update($request->except('products'));

        if ($request->has('categories')) {
            ProductCategory::query()->where(ProductCategoryContract::PRODUCT_ID, $product->id)->forceDelete();

            foreach ($request->categories as $item) {
                ProductCategory::query()->create([
                    ProductCategoryContract::CATEGORY_ID => $item,
                    ProductCategoryContract::PRODUCT_ID => $product->id,
                ]);
            }
        }

        if ($request->has('medias')) {
            ProductMedia::query()->where(ProductMediaContract::PRODUCT_ID, $product->id)->forceDelete();
            foreach ($request->medias as $media) {
                ProductMedia::query()->create([
                    ProductMediaContract::MEDIA => $media,
                    ProductMediaContract::TYPE => 'image',
                    ProductMediaContract::PRODUCT_ID => $product->id
                ]);
            }
        }

        return $product;
    }

    public function deleteProduct($id)
    {
        $product = Product::findOrFail($id);
        ProductMedia::query()->where(ProductMediaContract::PRODUCT_ID, $product->id)->forceDelete();
        ProductCategory::query()->where(ProductCategoryContract::PRODUCT_ID, $product->id)->forceDelete();
        $product->delete();
    }
}
