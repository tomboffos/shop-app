<?php

namespace App\Service\Shop\Product;

use App\Http\Requests\Shop\ProductGetRequest;

interface ProductServiceInterface
{
    public function getProducts(ProductGetRequest $request);

    public function getIdsOfChildCategories(int $categoryId): array;
}
