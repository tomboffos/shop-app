<?php

namespace App\Service\Shop\Recommendation;

use App\Contracts\Shop\RecommendationBloc\RecommendationCategoryContract;
use App\Contracts\Shop\RecommendationBloc\RecommendationContract;
use App\Contracts\Shop\RecommendationBloc\RecommendationProductContract;
use App\Http\Requests\RecommendationStoreRequest;
use App\Models\Recommendation;
use App\Models\RecommendationCategory;
use App\Models\RecommendationProduct;

class RecommendationService implements RecommendationServiceInterface
{

    public function getRecommendations()
    {
        return Recommendation::query()->orderBy(RecommendationContract::ORDER, 'asc')->get();
    }

    public function store(RecommendationStoreRequest $request)
    {
        $recommendation = Recommendation::query()->create($request->except(['categories', 'products']));

        if ($request->has('categories')) {
            foreach ($request->categories as $category) {
                RecommendationCategory::query()->create([
                    RecommendationCategoryContract::RECOMMENDATION_ID => $recommendation->id,
                    RecommendationCategoryContract::CATEGORY_ID => $category['id'],
                    RecommendationCategoryContract::ORDER => $category['order']
                ]);
            }
        }


        if ($request->has('products')) {
            foreach ($request->products as $product) {
                RecommendationProduct::query()->create([
                    RecommendationProductContract::RECOMMENDATION_ID => $recommendation->id,
                    RecommendationProductContract::PRODUCT_ID => $product['id'],
                    RecommendationProductContract::ORDER => $product['order']
                ]);
            }
        }


        return $recommendation;
    }

    public function update(int $id, RecommendationStoreRequest $request)
    {
        $recommendation = Recommendation::query()->findOrFail($id);

        $recommendation->update($request->except('categories', 'products'));

        if ($request->has('categories')) {
            RecommendationCategory::query()->where(RecommendationCategoryContract::RECOMMENDATION_ID, $id)->forceDelete();
            foreach ($request->categories as $category) {
                RecommendationCategory::query()->create([
                    RecommendationCategoryContract::RECOMMENDATION_ID => $recommendation->id,
                    RecommendationCategoryContract::CATEGORY_ID => $category['id'],
                    RecommendationCategoryContract::ORDER => $category['order']
                ]);
            }
        }


        if ($request->has('products')) {
            RecommendationProduct::query()->where(RecommendationProductContract::RECOMMENDATION_ID, $id)->forceDelete();
            foreach ($request->products as $product) {
                RecommendationProduct::query()->create([
                    RecommendationProductContract::RECOMMENDATION_ID => $recommendation->id,
                    RecommendationProductContract::PRODUCT_ID => $product['id'],
                    RecommendationProductContract::ORDER => $product['order']
                ]);
            }
        }


        return $recommendation;
    }


    public function delete($id)
    {
        $recommendation = Recommendation::query()->findOrFail($id);
        RecommendationProduct::query()->where(RecommendationProductContract::RECOMMENDATION_ID, $id)->forceDelete();
        RecommendationCategory::query()->where(RecommendationCategoryContract::RECOMMENDATION_ID, $id)->forceDelete();
        $recommendation->delete();

        return response('Deleted');
    }
}

