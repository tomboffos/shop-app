<?php

namespace App\Service\Shop\Recommendation;

interface RecommendationServiceInterface
{
    public function getRecommendations();
}
