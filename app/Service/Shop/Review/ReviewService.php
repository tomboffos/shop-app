<?php

namespace App\Service\Shop\Review;

use App\Contracts\Shop\Review\ReviewContract;
use App\Http\Requests\Shop\ReviewCreateRequest;
use App\Models\Product;
use App\Models\Review;

class ReviewService implements ReviewServiceInterface
{

    public function addReview(ReviewCreateRequest $request)
    {
        Review::query()->create(array_merge($request->validated(), [
            ReviewContract::USER_ID => $request->user()->id
        ]));

        return $this->getReviewsFromProduct(Product::query()->find($request->product_id));
    }

    public function getReviewsFromProduct(Product $product)
    {
        return Review::query()->where(ReviewContract::PRODUCT_ID, $product->id)->get();
    }
}
