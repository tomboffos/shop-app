<?php

namespace App\Service\Shop\Review;

use App\Http\Requests\Shop\ReviewCreateRequest;
use App\Models\Product;

interface ReviewServiceInterface
{
    public function addReview(ReviewCreateRequest $request);


    public function getReviewsFromProduct(Product $product);
}
