<?php

namespace App\Service;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class StoreService
{
    static function storeFile(UploadedFile $value): string
    {
        Log::info('File upload attempt: ', ['file' => $value]);

        // Store the file
        $filePath = $value->store('public');
        Log::info('File stored at: ', ['path' => $filePath]);

        // Generate the public URL
        $imagePath = str_replace('public', '', asset('storage/' . $filePath));
        Log::info('Generated image path: ', ['image_path' => $imagePath]);

        return $imagePath;

    }
}
