<?php

use App\Contracts\Shop\Products\ProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->text(ProductContract::NAME);
            $table->longText(ProductContract::DESCRIPTION)->nullable();
//            $table->text(ProductContract::CUSTOM_FIELDS)->nullable();
            $table->float(ProductContract::PRICE)->default(0);
            $table->foreignId(ProductContract::PER_PRICE_OPTION_ID)->constrained();
            $table->boolean(ProductContract::ACTIVE)->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
