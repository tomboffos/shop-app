<?php

use App\Contracts\Shop\Products\ProductMediaContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_media', function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductMediaContract::PRODUCT_ID)->constrained();
            $table->text(ProductMediaContract::MEDIA);
            $table->enum(ProductMediaContract::TYPE, ProductMediaContract::TYPE_VALUES)->default(ProductMediaContract::TYPE_VALUES[0]);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_media');
    }
}
