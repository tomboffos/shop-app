<?php

use App\Contracts\Shop\Products\CustomFields\CustomFieldCategoryContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId(CustomFieldCategoryContract::CATEGORY_ID)->constrained();
            $table->foreignId(CustomFieldCategoryContract::CUSTOM_FIELD_ID)->constrained();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_categories');
    }
}
