<?php

use App\Contracts\Profile\PushNotificationContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePushNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notifications', function (Blueprint $table) {
            $table->id();
            $table->text(PushNotificationContract::TITLE);
            $table->text(PushNotificationContract::SUBTITLE)->nullable();
            $table->longText(PushNotificationContract::DEVICES)->nullable();
            $table->enum(PushNotificationContract::TYPE, PushNotificationContract::TYPE_VALUES);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notifications');
    }
}
