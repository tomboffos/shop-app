<?php

use App\Contracts\Order\OrderContract;
use App\Contracts\Order\OrderStatusContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId(OrderContract::USER_ID)->nullable()->constrained();
            $table->text(OrderContract::COMMENTS)->nullable();
            $table->text(OrderContract::NAME)->nullable();
            $table->text(OrderContract::PHONE)->nullable();
            $table->foreignId(OrderContract::ORDER_STATUS_ID)->nullable();
            $table->double(OrderContract::PRICE);
            $table->text(OrderContract::ADDRESS)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
