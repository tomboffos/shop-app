<?php

use App\Contracts\Shop\Products\CustomFields\CustomFieldCategoryContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFilterToCustomFieldCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('custom_field_categories', function (Blueprint $table) {
            $table->boolean(CustomFieldCategoryContract::IS_FILTER)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('custom_field_categories', function (Blueprint $table) {
            $table->dropColumn(CustomFieldCategoryContract::IS_FILTER);
        });
    }
}
