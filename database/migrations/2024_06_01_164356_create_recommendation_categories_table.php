<?php

use App\Contracts\Shop\RecommendationBloc\RecommendationCategoryContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendation_categories', function (Blueprint $table) {
            $table->id();

            $table->foreignId(RecommendationCategoryContract::CATEGORY_ID)->constrained();
            $table->foreignId(RecommendationCategoryContract::RECOMMENDATION_ID)->constrained();
            $table->unsignedBigInteger(RecommendationCategoryContract::ORDER)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendation_categories');
    }
}
