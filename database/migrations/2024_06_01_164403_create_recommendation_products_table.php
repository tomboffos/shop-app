<?php

use App\Contracts\Shop\RecommendationBloc\RecommendationProductContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecommendationProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendation_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId(RecommendationProductContract::PRODUCT_ID)->constrained();
            $table->foreignId(RecommendationProductContract::RECOMMENDATION_ID)->constrained();
            $table->unsignedBigInteger(RecommendationProductContract::ORDER)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendation_products');
    }
}
