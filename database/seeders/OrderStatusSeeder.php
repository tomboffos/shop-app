<?php

namespace Database\Seeders;

use App\Contracts\Order\OrderStatusContract;
use App\Models\Order;
use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::query()->insert([
            OrderStatusContract::NAME => 'В обработке'
        ]);
    }
}
