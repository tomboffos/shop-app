<?php

namespace Database\Seeders;

use App\Contracts\Settings\SettingsContract;
use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhoneSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['id' => 2, 'name' => 'primary_color_dark', 'value' => '#000000'],
            ['id' => 4, 'name' => 'secondary_color_dark', 'value' => '#000000'],
            ['id' => 6, 'name' => 'logo', 'value' => 'http://127.0.0.1:8000/storage/jw1oroA...'],
            ['id' => 10, 'name' => 'delivery_price', 'value' => '200'],
            ['id' => 14, 'name' => 'text_color', 'value' => 'FF1F1C1C'],
            ['id' => 8, 'name' => 'icon_color', 'value' => 'FF000000'],
            ['id' => 11, 'name' => 'drawer', 'value' => 'false'],
            ['id' => 16, 'name' => 'text_theme', 'value' => 'openSans'],
            ['id' => 13, 'name' => 'filled_button_color_dark', 'value' => 'FFE31B1B'],
            ['id' => 3, 'name' => 'secondary_color_light', 'value' => 'FFFAFFFD'],
            ['id' => 1, 'name' => 'primary_color_light', 'value' => 'FFFCFCFC'],
            ['id' => 9, 'name' => 'border_color_light', 'value' => 'FFFF00FF'],
            ['id' => 12, 'name' => 'icon_color_dark', 'value' => '#000000'],
            ['id' => 15, 'name' => 'text_color_dark', 'value' => '#000000'],
            ['id' => 17, 'name' => 'border_color_dark', 'value' => '#ffffff'],
            ['id' => 18, 'name' => 'filled_button_color_dark', 'value' => '#ffffff'],
            ['id' => 5, 'name' => 'title', 'value' => 'ZHOPASs'],
            ['id' => 7, 'name' => 'filled_button_color', 'value' => 'FFCED6D9'],
        ];

        foreach ($settings as $setting) {
            DB::table('settings')->updateOrInsert(
                ['id' => $setting['id']],
                [
                    'name' => $setting['name'],
                    'value' => $setting['value'],
                    'deleted_at' => null,
                    'created_at' => now(),
                ]
            );
        }
    }
}
