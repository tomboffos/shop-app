<?php

namespace Database\Seeders;

use App\Contracts\Profile\RoleContract;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            ['name' => 'primary_color_light', 'value' => '#E1F5FE'],
            ['name' => 'primary_color_dark', 'value' => '#000000'],
            ['name' => 'secondary_color_light', 'value' => '#ffffff'],
            ['name' => 'secondary_color_dark', 'value' => '#000000'],
            ['name' => 'title', 'value' => 'ZHOPA'],
            ['name' => 'logo', 'value' => 'https://static.vecteezy.com/system/re...'],
            ['name' => 'filled_button_color', 'value' => '#B3E5FC'],
            ['name' => 'icon_color', 'value' => '#000000'],
            ['name' => 'border_color_light', 'value' => '#ffffff'],
            ['name' => 'delivery_price', 'value' => '200'],
            ['name' => 'drawer', 'value' => 'false'],
        ];

        foreach ($settings as $setting) {
            // Check if the setting already exists
            $exists = DB::table('settings')->where('name', $setting['name'])->exists();

            if (!$exists) {
                // Insert the setting if it doesn't exist
                DB::table('settings')->insert([
                    'name' => $setting['name'],
                    'value' => $setting['value'],
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
    }
}
