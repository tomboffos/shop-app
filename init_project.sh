# Get the container ID of the Laravel application container
CONTAINER_ID=$(docker-compose ps -q laravel.test) &&docker exec -it $CONTAINER_ID cp .env.example .env && docker exec -it $CONTAINER_ID composer install && docker exec -it $CONTAINER_ID php artisan key:generate && docker exec -it $CONTAINER_ID php artisan migrate && docker exec -it $CONTAINER_ID php artisan db:seed

ECHO $CONTAINER_ID
