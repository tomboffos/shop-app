<?php

use App\Http\Controllers\Api\Advertisement\BannerController;
use App\Http\Controllers\Api\Advertisement\NewsController;
use App\Http\Controllers\Api\Order\OrderController;
use App\Http\Controllers\Api\Profile\AuthController;
use App\Http\Controllers\Api\Profile\DeviceController;
use App\Http\Controllers\Api\Setting\SettingController;
use App\Http\Controllers\Api\Shop\CategoryController;
use App\Http\Controllers\Api\Shop\CustomFieldController;
use App\Http\Controllers\Api\Shop\ProductController;
use App\Http\Controllers\Api\Shop\ProductHistoryController;
use App\Http\Controllers\Api\Shop\RecommendationController;
use App\Http\Controllers\Api\Shop\ReviewController;
use App\Http\Controllers\Api\Shop\PerPriceOptionController;
use App\Http\Resources\Profile\AuthResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * @OA\Info(
 *     title="Shop API",
 *     version="1.0.0",
 *     description="API documentation for the Shop application"
 * )
 *
 * @OA\Server(
 *     url=L5_SWAGGER_CONST_HOST,
 *     description="API server"
 * )
 */

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('device', [DeviceController::class, 'create']);
    Route::post('admin/notification', [DeviceController::class, 'send'])->middleware('auth:sanctum');
    Route::get('user', function (Request $request) {
        if (env('APP_DEBUG')) {
            $user = \App\Models\User::query()->where(\App\Contracts\Profile\UserContract::ROLE_ID, 2)->first();
            if ($user == null) {
                \App\Models\User::query()->create([
                    \App\Contracts\Profile\UserContract::NAME => 'admin',
                    \App\Contracts\Profile\UserContract::EMAIL => 'admin@admin.ru',
                    \App\Contracts\Profile\UserContract::PASSWORD => 'password',
                    \App\Contracts\Profile\UserContract::ROLE_ID => 2
                ]);

                return new AuthResource($user,);
            }

            return new AuthResource($user,);
        }


        return new AuthResource($request->user(), false);
    });
});

Route::prefix('shop')->group(function () {
    // Category routes

    Route::prefix('category')->group(function () {
        Route::prefix('admin')->group(function () {
            Route::post('', [CategoryController::class, 'store']);
            Route::get('', [CategoryController::class, 'getAdminCategory']);
            Route::post('{id}', [CategoryController::class, 'update']);
            Route::delete('{id}', [CategoryController::class, 'destroy']);
        });
        Route::get('{categoryId?}', [CategoryController::class, 'getCategories']);


    });

    // Product routes
    Route::prefix('products')->group(function () {
        Route::post('', [ProductController::class, 'getProducts']);
        Route::prefix('admin')->group(function () {
            Route::post('', [ProductController::class, 'store']);
            Route::post('{id}', [ProductController::class, 'update']);
            Route::delete('{id}', [ProductController::class, 'destroy']);
        });
    });

    // Per Price Option routes
    Route::prefix('per-price-options')->group(function () {
        Route::get('', [PerPriceOptionController::class, 'index']);
        Route::prefix('admin')->group(function () {
            Route::post('', [PerPriceOptionController::class, 'store']);
            Route::post('{id}', [PerPriceOptionController::class, 'update']);
            Route::delete('{id}', [PerPriceOptionController::class, 'destroy']);
        });
    });


    Route::prefix('recommendations')->group(function () {
        Route::get('/', [RecommendationController::class, 'index']);
        Route::prefix('admin')->group(function () {
            Route::post('/', [RecommendationController::class, 'store']);
            Route::post('/{id}', [RecommendationController::class, 'update']);
            Route::delete('/{id}', [RecommendationController::class, 'delete']);
        });
    });


    Route::prefix('filters')->group(function () {
        Route::prefix('admin')->group(function () {
            Route::get('', [CustomFieldController::class, 'getAdmin']);
            Route::post('', [CustomFieldController::class, 'store']);
            Route::post('{id}', [CustomFieldController::class, 'update']);
            Route::delete('{id}', [CustomFieldController::class, 'delete']);
        });

        Route::get('{category}', [CustomFieldController::class, 'getCategories']);


    });

    Route::prefix('product-history')->group(function () {
        Route::get('{device}', [ProductHistoryController::class, 'index']);
        Route::post('add', [ProductHistoryController::class, 'store']);
    });

    Route::prefix('review')->group(function () {
        Route::get('/{product}', [ReviewController::class, 'index']);
        Route::post('', [ReviewController::class, 'store'])->middleware('auth:sanctum');
    });

    Route::prefix('order')->group(function () {
        Route::get('', [OrderController::class, 'index']);
        Route::post('', [OrderController::class, 'store']);

        Route::get('admin', [OrderController::class, 'getAdmin']);
    });
});

Route::prefix('advertisement')->group(function () {
    Route::get('news', [NewsController::class, 'index']);
    Route::get('banner', [BannerController::class, 'index']);
});

Route::get('settings', [SettingController::class, 'index']);
Route::prefix('admin')->group(function () {
    Route::post('settings', [SettingController::class, 'update']);
});
