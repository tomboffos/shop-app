<?php

namespace Tests\Feature\Order;

use App\Contracts\Order\OrderContract;
use App\Models\Device;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderTest extends TestCase
{
    private $deviceId;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create()
    {
        $this->deviceId = Device::query()->inRandomOrder()->first()->id;

        $response = $this->postJson('/api/shop/order', [
            OrderContract::PHONE => '+77475039809',
            OrderContract::ADDRESS => 'Almaty',
            OrderContract::NAME => 'Assyl',
            OrderContract::DEVICE_ID => $this->deviceId,
            'products' => [
                [
                    'product_id' => Product::query()->inRandomOrder()->first()->id,
                    'quantity' => 10
                ]
            ]
        ]);


        $response->assertStatus(201);
    }


    public function test_get()
    {
        $response = $this->get('/api/shop/order?device_id=18');

        $response->assertStatus(200);
    }
}
