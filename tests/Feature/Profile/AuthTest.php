<?php

namespace Tests\Feature\Profile;

use App\Contracts\Profile\DeviceContract;
use App\Contracts\Profile\UserContract;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class AuthTest extends TestCase
{

    public function test_register_success()
    {
        $rand = Str::random(10);

        $response = $this->postJson('/api/auth/register', [
            UserContract::EMAIL => "$rand@gmail.com",
            UserContract::PASSWORD => '11072000a',
            UserContract::NAME => 'Asyl',
            DeviceContract::TOKEN => 'token'
        ]);

        $response->assertStatus(201);
    }


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login_success()
    {
        $user = User::query()->inRandomOrder()->first();

        $response = $this->postJson('/api/auth/login', [
            UserContract::EMAIL => $user->email,
            UserContract::PASSWORD => '11072000a',
            DeviceContract::TOKEN => 'token'
        ]);

        $response->assertStatus(200);
    }

    public function test_login_failure()
    {
        $response = $this->postJson('/api/auth/login', [
           UserContract::EMAIL => 'tomboffos@gmail.com',
           UserContract::PASSWORD => '11072000',
           DeviceContract::TOKEN => 'token'
        ]);

        $response->assertStatus(404);
    }


    public function test_register_failure()
    {
        $response = $this->postJson('/api/auth/register', [
           UserContract::EMAIL => ''
        ]);

        $response->assertStatus(422);
    }
}
