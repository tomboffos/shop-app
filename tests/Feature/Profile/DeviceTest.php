<?php

namespace Tests\Feature\Profile;

use App\Contracts\Profile\DeviceContract;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeviceTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_device_create()
    {
        $response = $this->postJson('/api/auth/device', [
            DeviceContract::TOKEN => 'token'
        ]);

        $response->assertStatus(201);
    }
}
