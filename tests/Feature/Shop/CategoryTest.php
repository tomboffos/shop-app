<?php

namespace Tests\Feature\Shop;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_parent_get()
    {
        $response = $this->get('/api/shop/category');


        $response->assertStatus(200);
    }


    public function test_child_get()
    {
        $category = Category::query()->find(1);

        $response = $this->get('/api/shop/category/'.$category->id);

        $response->assertStatus(200);
    }
}
