<?php

namespace Tests\Feature\Shop;

use App\Contracts\Shop\Products\ProductHistoryContract;
use App\Models\Device;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductHistoryTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_history()
    {
        $device = Device::query()->find(1);

        $response = $this->get('/api/shop/product-history/' . $device->id);

        $response->assertStatus(200);
    }


    public function test_add_history()
    {
        $product = Product::query()->inRandomOrder()->first();

        $response = $this->postJson('/api/shop/product-history/add', [
            ProductHistoryContract::PRODUCT_ID => $product->id,
            ProductHistoryContract::DEVICE_ID => 1
        ]);

        $response->assertStatus(201);
    }
}
