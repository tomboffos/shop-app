<?php

namespace Tests\Feature\Shop;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_products()
    {
        $response = $this->postJson('/api/shop/products');

        $response->assertStatus(200);
    }


    public function test_get_products_by_category()
    {
        $response = $this->postJson('/api/shop/products', [
            'product_id' => 3
        ]);


        $response->assertStatus(200);
    }


    public function test_get_products_by_parent()
    {
        $response = $this->postJson('/api/shop/products', [
            'product_id' => 1,
            'filters' => [
                'ozu' => 16,
            ]
        ]);


        $response->assertStatus(200);
    }
}
