<?php

namespace Tests\Feature\Shop;

use App\Contracts\Shop\Review\ReviewContract;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create()
    {
        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.User::query()->find(1)->createToken(env('APP_NAME'))->plainTextToken
        ])->postJson('/api/shop/review', [
            ReviewContract::RATING => 5.0,
            ReviewContract::TEXT => 'asd',
            ReviewContract::PRODUCT_ID => Product::query()->inRandomOrder()->first()->id
        ]);

        $response->assertStatus(200);
    }


    public function test_get()
    {
        $product = Product::query()->inRandomOrder()->first();

        $response = $this->get('/api/shop/review/'.$product->id);

        $response->assertStatus(200);
    }
}
